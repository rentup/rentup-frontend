import express from 'express';
import router from 'app/router';
import cookieParser from 'cookie-parser';

const app = express();

// Client side script are served
app.use(express.static('public'));

app.use(cookieParser());

app.use('/', router);

app.listen(3000, () => {
    console.log('Listening on port 3000')
});
