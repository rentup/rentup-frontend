const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base');

const config = {
    entry: {
        'home': './app/client/home/index.js',
        'login': './app/client/login/index.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'public'),
    },
};

module.exports = merge(baseConfig, config);