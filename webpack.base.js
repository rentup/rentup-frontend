const path = require("path");
const { NODE_ENV } = process.env;

module.exports = {
    resolve: {
        extensions: ['.js', '.jsx', 'scss'],
        alias: {
            'app': path.resolve(__dirname, './app'),
            'services': path.resolve(__dirname, './services'),
        },
    },
    module: {
        rules: [
            {
                test: [/\.js?$/, /\.jsx?$/],
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: [
                        '@babel/preset-react',
                        '@babel/preset-env'
                    ],

                    plugins: ['@babel/plugin-proposal-class-properties']
                }
            },
            {
                test: /\.scss$/,
                use: [
                    'isomorphic-style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                ]
            }
        ],
    }
}