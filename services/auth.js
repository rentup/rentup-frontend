import ApiClient from 'services/ApiClient';
import getConfig from 'services/utils/config';
import Cookies from 'js-cookie';


class AuthService {
    constructor(req = {}) {
        // const { basePath, headers } = getConfig(req);
        this.apiClient = new ApiClient().connect({req});
    }

    login = async ({ username, password }) => {
        const response = await this.apiClient.post({
            apiName: 'login',
            data: {
                username,
                password
            },
        });
        Cookies.set('access_token', response.data.access_token);
        Cookies.set('refresh_token', response.data.refresh_token);
        return response;
    }

    getUser = async () => {
        const response = await this.apiClient.get({
            apiName: 'user/5e62b4430fc5d3874c546380',
        });
        return response;
    } 
}



export default AuthService;