import axios from 'axios';
import jwtDecode from 'jwt-decode';
import Cookies from 'js-cookie';
import { getBasePath } from 'services/utils/config';

const setAuthInterceptors = () => {
    axios.interceptors.request.use(request => {
        const basePath = getBasePath();
        const apiName = request.url.replace(basePath, '');
        if (apiName != '/login' && apiName != '/token/refresh') {
            // The exp time is on seconds
            const token = Cookies.get('access_token');
            
            // If token is undefined it's a server side request;
            if (!token) return request;

            const decoded = jwtDecode(token);

            //We convert todays date to seconds
            const timeNow = new Date().getTime() / 1000;

            // If token is expired, we update it
            if (decoded.exp < timeNow) {
                return getNewToken()
                    .then((data) => {
                        // The old request is return with new token
                        request.headers['X-Access-Token'] = data.access_token;
                        Cookies.set('access_token', data.access_token);
                        return request;
                    })
                    .catch((error) => {
                        Promise.reject(error);
                    });
            }
        }
        return request;
    });
};


const getNewToken = () => {
    const refreshToken = Cookies.get('refresh_token');
    const basePath = getBasePath();
    return new Promise((resolve, reject) => {
        axios.post(`${basePath}/token/refresh`, {
            refresh_token: refreshToken
        }).then(response => {
            resolve(response.data);
        }).catch((e) => {
            reject(e);
        })
    });
}

export {
    setAuthInterceptors,
};