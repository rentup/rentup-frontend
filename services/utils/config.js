import Cookies from 'js-cookie';

const { NODE_ENV } = process.env;
const production = NODE_ENV === 'production';

const getHeaders = (req) => {
    return {
        'X-Access-Token': (req && req.cookies) ? req.cookies['access_token'] : Cookies.get('access_token'),
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    }
};

const getBasePath = () => production ? null : 'http://localhost:8080'

export {
    getHeaders,
    getBasePath
}