import '@babel/polyfill/noConflict';
import axios from 'axios';
import { setAuthInterceptors } from 'services/utils/interceptors';
import { getBasePath, getHeaders } from 'services/utils/config';

class ApiClient {
    constructor() {
        this.basePath = getBasePath();
        setAuthInterceptors();
    }


    requestFactory = ({ method, baseUrl, headers, params = {}, data = {} }) => {
        if (method === 'post' || method === 'put') {
            return axios[method](baseUrl, data, {
                params,
                headers,
            });
        }
        return axios[method](baseUrl, {
            data,
            params,
            headers,
        });
    }

    dispatch = async ({ method, req, apiName, customHeaders = {}, ...paramsRequest }) => {
        const baseUrl = `${this.basePath}/${apiName}`;
        const headers = { ...customHeaders, ...getHeaders(req) };
        try {
            const response = await this.requestFactory({ method, baseUrl, headers, ...paramsRequest });
            return response;
        } catch (e) {
            throw new Error(e);
        }
    }

    connect = ({ req = {}, customHeaders }) => ({
        get: async ({ apiName, ...paramsRequest }) => await this.dispatch({
            method: 'get',
            req,
            apiName,
            customHeaders,
            ...paramsRequest
        }),
        post: async ({ apiName, ...paramsRequest }) => await this.dispatch({
            method: 'post',
            req,
            apiName,
            customHeaders,
            ...paramsRequest
        }),
        put: async ({ apiName, ...paramsRequest }) => await this.dispatch({
            method: 'put',
            req,
            apiName,
            customHeaders,
            ...paramsRequest
        }),
        delete: async ({ apiName, ...paramsRequest }) => await this.dispatch({
            method: 'delete',
            req,
            apiName,
            customHeaders,
            ...paramsRequest
        })
    });
}

export default ApiClient;