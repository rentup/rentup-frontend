
import express from 'express';
import { authMiddleware, authErrorMiddleware } from 'app/router/middleware';
import HomeRoute from 'app/pages/home';
import LoginRouter from 'app/pages/login';

const router = express.Router();


// Routes to the pages are declared
router.use(authMiddleware);
router.use(authErrorMiddleware);
router.use('/home', HomeRoute);
router.use('/login', LoginRouter);
// router.use('/', (req, res) => res.redirect('/login'));

export default router;
