import '@babel/polyfill'
import jwt from 'express-jwt';
import axios from 'axios';
import { getBasePath } from 'services/utils/config';

const getToken = (req) => req.cookies['access_token'];

const authMiddleware = (req, res, next) => {
    jwt({
        secret: '123456a',
        getToken,
    }).unless({ path: ['/login'] })(req, res, next);
}

const authErrorMiddleware = async (err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        if (!!req.cookies['refresh_token']) {
            const basePath = getBasePath();
            try {
                const response = await axios.post(`${basePath}/token/refresh`, {
                    refresh_token: req.cookies['refresh_token']
                });
                req.cookies.access_token = response.data.access_token;
                res.cookie('access_token', response.data.access_token);
                return next();
            } catch (e) {
                res.redirect('/login');
                return next();
            }
        }
        res.redirect('/login');
    }
};

export {
    authMiddleware,
    authErrorMiddleware
}