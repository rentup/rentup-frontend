import React from 'react';
import serialize from 'serialize-javascript';
import StyleContext from 'isomorphic-style-loader/StyleContext'
import { ServerStyleSheets, StylesProvider, ThemeProvider } from '@material-ui/core/styles';
import { renderToString } from 'react-dom/server';
import theme from 'app/styles/theme';

const getTemplate = (stringComponent, props, css, cssMUI) => (
    `
    <html>
        <head>
            <title> Rent Up </title>
            <style id="jss-server-side">${cssMUI}</style>
            <style>${[...css].join('')}</style>
            <script id="props-client-side">
                window.INITIAL_STATE = ${serialize(props, { isJSON: true })}
            </script>
        </head/>
        <body>
            ${stringComponent}
    </body>
     </html>
    `
);

const renderView = (Component, props = {}) => (req, res) => {
    const css = new Set(); // CSS for all rendered React components
    const insertCss = (...styles) => styles.forEach(style => css.add(style._getCss()));

    const sheets = new ServerStyleSheets();
    
    const stringComponent = renderToString(
        sheets.collect(
            <main id="root">
                <ThemeProvider theme={theme}>
                    <StylesProvider injectFirst>
                        <StyleContext.Provider value={{ insertCss }}>
                            <Component {...props} />
                        </StyleContext.Provider>
                    </StylesProvider>
                </ThemeProvider>
            </main>
        )
    );

    // Grab the CSS from the sheets.
    const cssMUI = sheets.toString();
    res.send(getTemplate(stringComponent, props, css, cssMUI));
};

export default renderView;