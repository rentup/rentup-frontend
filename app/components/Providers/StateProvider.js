import React from 'react';

const initialState = {
    state: {},
    actions: {},
};

const store = React.createContext(initialState);
const { Provider } = store;

const StateProvider = ({ children, rawActions }) => {
    const [state, setState] = React.useState({});
    const contextBinding = {
        state,
        actions: rawActions({
            dispatch: setState,
            state: state
        })
    }

    return (
        <Provider value={contextBinding}>
            {children}
        </Provider>
    );
};

export { 
    store, 
    StateProvider 
};