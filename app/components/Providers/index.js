import { store, StateProvider } from 'app/components/Providers/StateProvider';

export {
    store,
    StateProvider
}