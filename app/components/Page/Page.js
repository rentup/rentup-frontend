import React from 'react';
import classNames from 'classnames';
import withStyles from 'isomorphic-style-loader/withStyles'
import CssBaseline from '@material-ui/core/CssBaseline';
import s from './page.scss'

const Page = ({ children, name, className }) => (
    <CssBaseline>
        <div
            className={classNames('rentup-page', name, className)}
        >
            {children}
            <script src={`${name}.js`} />
        </div>
    </CssBaseline>
);

export default withStyles(s)(Page);