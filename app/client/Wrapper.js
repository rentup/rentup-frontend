import React from 'react';
import { ThemeProvider, StylesProvider } from '@material-ui/core/styles';
import StyleContext from 'isomorphic-style-loader/StyleContext'
import theme from 'app/styles/theme';

const Wrapper = ({ children }) => {
    React.useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side');
        const preloadedProps = document.querySelector('#props-client-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
        if(preloadedProps) {
            preloadedProps.parentElement.removeChild(preloadedProps);
        }
    }, []);

    const insertCss = (...styles) => {
        const removeCss = styles.map(style => style._insertCss())
        return () => removeCss.forEach(dispose => dispose())
    }

    return (
        <ThemeProvider theme={theme}>
            <StylesProvider injectFirst>
                <StyleContext.Provider value={{ insertCss }}>
                    {children}
                </StyleContext.Provider>
            </StylesProvider>
        </ThemeProvider>
    );
}

export { Wrapper }