import '@babel/polyfill/noConflict';
import React from 'react';
import ReactDOM from 'react-dom';
import { Wrapper } from './Wrapper';

const Initializer = (Component) => {
    const props = window.INITIAL_STATE;
    ReactDOM.hydrate(
        <Wrapper>
            <Component {...props} />
        </Wrapper>,
        document.querySelector('#root')
    )
}

export default Initializer;
