import express from 'express';
import { render, fetchUsers } from 'app/pages/home/controller';

const router = express.Router();

router.use('/', fetchUsers, render);

export default router;