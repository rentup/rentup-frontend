import React from 'react';
import { StateProvider, store } from 'app/components/Providers';
import rawActions from 'app/pages/home/actions';
import TextField from '@material-ui/core/TextField';
import { Page } from 'app/components/Page';
import s from '../styles/home.scss';

const HomePage = (props) => {
    const { state, actions } = React.useContext(store);
    return (
        <Page name={'home'}>
            <h1 className={'test'}>I'm the Home component</h1>
            <button onClick={actions.getUser}>Click me</button>
            {state.user &&
                <h1>{`${state.user.name} ${state.user.surname}`}</h1>
            }
        </Page>
    )
};

const withProviders = props => (
    <StateProvider rawActions={rawActions}>
        <HomePage {...props} />
    </StateProvider>
);

export default withProviders;
