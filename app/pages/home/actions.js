import AuthService from 'services/auth';

const authService = new AuthService();

const actions = ({ state, dispatch }) => {
    return {
        getUser: async () => {
            const user = await authService.getUser();
            dispatch({
                ...state,
                user: user.data,
            });
        },
    };
};

export default actions;