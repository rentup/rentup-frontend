import '@babel/polyfill/noConflict';
import View from 'app/pages/home/views/HomePage';
import renderView from 'app/utils/renderer';
import AuthService from 'services/auth';



const fetchUsers = async (req, res, next) => {
    const authService = new AuthService(req);
    try {
        res.locals.user = await authService.getUser();
    } catch (e) {
        console.log(e);
    }
    next();
}

const render = (req, res) => {
    const extraProps = {
        user: res.locals.user.data,
    }
    renderView(View, extraProps)(req, res);
}

export {
    render,
    fetchUsers
}
