import express from 'express';
import { render } from 'app/pages/login/controller';

const router = express.Router()

router.use('/', render);

export default router;