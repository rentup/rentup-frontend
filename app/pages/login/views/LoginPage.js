/**
 * Styles
 */
import s from 'app/pages/login/styles/login.scss';

/**
 * Utils
 */
import React from 'react';
import withStyles from 'isomorphic-style-loader/withStyles'
import rawActions from 'app/pages/login/actions';

/**
 * Components
 */
import { Page } from 'app/components/Page';
import { StateProvider, store } from 'app/components/Providers';
import { Card, Button, TextField, CircularProgress } from '@material-ui/core';

const LoginPage = () => {
    const { state, actions } = React.useContext(store);
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');

    return (
        <Page name='login'>
            <Card classes={{ root: "login__card__container" }}>
                {state.loading ?
                    (<CircularProgress />)
                    : (
                        <React.Fragment>
                            <TextField
                                id="outlined-required"
                                label="Email"
                                className="input"
                                variant="outlined"
                                onChange={(e) => setUsername(e.target.value)}
                            />
                            <TextField
                                id="outlined-password-input"
                                className="input"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                variant="outlined"
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => actions.loginUser({ username, password })}
                            >
                                Login
                            </Button>
                        </React.Fragment>
                    )
                }
            </Card>
        </Page>
    );
};

const withProvider = props => (
    <StateProvider rawActions={rawActions}>
        <LoginPage {...props} />
    </StateProvider>
);

export default withStyles(s)(withProvider);
