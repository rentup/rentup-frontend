import AuthService from 'services/auth';

const authService = new AuthService();

const actions = ({ state, dispatch }) => {
    return {
        loginUser: async ({ username, password }) => {
            dispatch({
                loading: true,
            })
            try {
                await authService.login({ username, password });
                window.location.href = '/home';
            } catch (e) {
                console.log(e);
                dispatch({ loading: false })
            }
        },
    };
};

export default actions;