import View from 'app/pages/login/views/LoginPage';
import renderView from 'app/utils/renderer';

const render = (req, res) => {
    renderView(View)(req, res);
}

export {
    render
}
